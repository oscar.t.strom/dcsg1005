# Group Project (40% of your grade)

## Your Task

Your project task is to implement an Active Directory for your choice of
organization:

1. Make up a small or medium size artificial organzation/company and their
   organizational chart, search the internet for "organisasjonskart", find
   inspiration from
   [SSB](https://www.ssb.no/nasjonalregnskap-og-konjunkturer/faktaside/norsk-naeringsliv),
   [gulesider](https://www.gulesider.no/firma),
   [BDB.no](https://www.bdb.no/Bransjesok.aspx), etc

2. Make a nice looking **simple** organizational chart in png-format for your organization

3. Design and implement the organizational structure in Active Directory on
   `dc1` using one or more PowerShell scripts. Join `mgr`, `cl1` and `srv1` to
   the domain. Add OUs, users, groups and group policy.

4. Create a short video (max length five minutes) where you demonstrate how it
   works. The video should be a screen recording with voice where you show that
   you can run your scripts/powershell commands (or maybe somtimes cmd-commands
   if necessary) successfully. The video should start with a fresh Active
   Directory installation (meaning you do not have to show that you do
   `Install-ADDSForest` or that you join the computers to the domain).

5. Note: this is a small but formal _project_, use what you have learned in
   DCSG1002 about team and project work:

    * make a very short project plan
    * assign team roles (if more than one person)
    * have a few meetings with very short concrete minutes (if more than one person)

## Practical Details

1. Join together to form project groups of one, two, three, four or five
   students in each group. Each student should spend approximately 30 hours on this
   project.

2. Make a **PRIVATE** fork of [the template git
   repository](https://gitlab.stud.idi.ntnu.no/erikhje/dcsg1005-template). Add
   the user `erikhje` **with role Developer**. Do not change any file names in
   this repo except for in the `code` directory (your PowerShell scripts) and in
   the `img` directory (organizational chart and other figures/illustrations):

    * The README-file should only contain a link to your video
    * The REPORT-file should contain your report
    * The MINUTES-file should contain plan, roles and minutes (if more than one person in the group)

3. **You have to hand-in in Inspera by the deadline March 21st 14:00 the
   following**:
    * _a PDF-file containing only the URL to your git repository AND the URL to your video_
    * _a zip-file of your entire git repository_

## Grading Criteria

You will be graded based on

* The git commit history
  * all team members (if more than one) contributed?
  * sensible commit messages?
* The project and team work
  * to-the-point meetings with "what have been done, what to do until next meeting"
* The report
  * approximately 1200 words describe which challenges you have faced and how you have
    solved them, use [inline-style links like
    this](https://docs.gitlab.com/ee/user/markdown.html#links) to any source you
    have used instead of formal citations with a bibliography.
* The PowerShell scripts
  * are the scripts linted with psscriptanalyzer?
  * readable code with proper indentation?
  * proper use of loops and/or pipelines to avoid repetition?
  * proper level of comments (not too many or too few)?
  * AD design mathces the organization?
  * users and groups makes sense?
  * sensible group policy objects?
* The video
  * informative and to the point but with sufficient detail?
* Overall impression and creativity. If you want to get a better grade than a C,
  read what is stated about grades A and B in [Karakterbeskrivelser for
  teknologiske
  fag](https://i.ntnu.no/wiki/-/wiki/Norsk/Karakterbeskrivelser+for+teknologiske+fag)

And of course note: [YOU HAVE TO WRITE THE REPORT IN YOUR OWN
  WORDS](https://innsida.ntnu.no/wiki/-/wiki/English/Cheating+on+exams) and **if
  you reuse code from the internet or from other students you have to clearly
  indicate this with a comment in your code giving credit to the original
  source.**

## How do I get help?

Contact Peder, Jan or Erik any time in Teams (we might not respond immediately)
or in the weekly timeslots in the time schedule.
